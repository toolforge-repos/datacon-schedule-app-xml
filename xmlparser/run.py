import logging
from .model import SimpleTZ
from .importers import importers
from .exporters import exporters

def main():
    logging.basicConfig(level=logging.INFO, format='%(message)s')
    
    json_file_path = '/data/project/datacon-schedule-app/app-schedule/wikiconvention-schedule-app/sessions.json'
    xml_file_path = '/data/project/datacon-schedule-app/app-schedule/wikiconvention-schedule-app/conference.xml'

    json_file = open(json_file_path, "r")
    xml_file = open(xml_file_path, "w")
    
    schedule = None
    conf = None
    imp = importers[0]
    conf = imp.parse(json_file)
    conf.prepare()
    if conf is None:
        logging.error('Error: could not determine format for %s', i.name)
    if conf.needs_data:
        logging.error('Cannot instantiate the schedule, it needs base data like title')
    else:
        schedule = conf

    if schedule is None:
        logging.warning('No schedule to export')
    elif schedule.is_empty():
        logging.warning('Schedule is empty')
    else:
        exporters['xml'].write(xml_file, schedule)

    json_file.close()
    xml_file.close()


if __name__ == '__main__':
    main()

