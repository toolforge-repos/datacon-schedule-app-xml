from ..model import Conference, Room, Speaker, Event
from datetime import datetime
import json
import re


DATE_ISO_FORMAT = '%Y-%m-%dT%H:%M:%S'


def parse_time(timestr):
    if 'T' in timestr:
        return datetime.strptime(timestr, DATE_ISO_FORMAT)
    raise ValueError('Unknown format: {}'.format(timestr))


def raw_text(html):
    _r = re.compile('<.*?>')
    return re.sub(_r, '', html)

# Forked from https://github.com/voc/schedule/blob/24af1028ebb93116a05edd200b3a21e980df9e1a/voc/tools.py#L71-L79
def normalise_string(string):
    string = string.lower()
    string = string.replace(u"ä", 'ae')
    string = string.replace(u'ö', 'oe')
    string = string.replace(u'ü', 'ue')
    string = string.replace(u'ß', 'ss')
    string = re.sub('\W+', '\_', string.strip()) # replace whitespace with _
    # string = filter(unicode.isalnum, string)
    string = re.sub('[^a-z0-9_]+', '', string) # TODO: is this not already done with \W+  line above?
    return string


def extract_id(id):
    if re.findall('^SUB-', id):
        return int('1' + id[4:])
    elif re.findall('^SE-', id):
        return int(id[3:])
    return id


class MozfestJsonImporter:
    name = 'mozfest'

    __timeblocks = {
        'tb-friday': '2019-10-25',
        'tb-saturday': '2019-10-26',
    }

    def parse(self, fileobj):
        data = json.load(fileobj)
        conf = Conference('Wikidatacon 2019')
        conf.slug = 'wikidatacon2019'
        conf.url = 'https://program.wikidatacon.org'
        xid = 1
        for xevent in data['sessions']:
            event = Event(xevent['title'], id=extract_id(xevent['id']))
            event.room = Room(xevent['location'])
            xtime = None
            if xevent['timeblock'] in self.__timeblocks:
                xtime = self.__timeblocks[xevent['timeblock']] + 'T' + xevent['start'] + ':00'
            event.start = parse_time(xtime)
            xduration = xevent['duration'].replace('min', '')
            event.duration = int(xduration)
            event.slug = '{slug}-{id}-{name}'.format(
                slug=conf.slug.lower(),
                id=event.id,
                name=normalise_string(event.title.lower())
            )
            event.url = xevent['link']
            event.description = raw_text(xevent['description'].replace('\x0D', ''))
            for xsp in xevent['facilitator_array']:
                event.speakers.append(Speaker(raw_text(xsp)))
            conf.events.append(event)
            xid += 1
        return conf
