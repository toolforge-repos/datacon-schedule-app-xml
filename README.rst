Schedule Convert
================

Use this to convert any schedule to a frab-compatible XML.

Author and License
------------------

Forked from https://github.com/Zverik/schedule-convert, originaly made by Ilya Zverev, published under MIT license.

Reworked by Jitrixis.
